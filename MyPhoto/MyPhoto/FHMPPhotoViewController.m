//
//  FHMPPhotoViewController.m
//  MyPhoto
//
//  Created by Fabio Barboza on 1/20/14.
//  Copyright (c) 2014 Fabio Barboza. All rights reserved.
//

#import "FHMPPhotoViewController.h"
#import "UIImageResizing.h"

@interface FHMPPhotoViewController ()
@property (weak, nonatomic) IBOutlet UIScrollView *scroll;
@property (nonatomic) UIImageView *photoContent;
@property (nonatomic) UISlider *slider;
@property (nonatomic) UISwitch *switchBtn;

@end

@implementation FHMPPhotoViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    _scroll.delegate = self;
    _scroll.minimumZoomScale = 1.0f;
    _scroll.maximumZoomScale = 6.0f;
    [self createComponents];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)createComponents
{
    _photoContent = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320, 440)];
    _photoContent.contentMode = UIViewContentModeScaleAspectFit;
    _photoContent.image = _currentPhoto.image;
    [_photoContent setBackgroundColor:[UIColor colorWithRed:40/255.0f green:40/255.0f blue:40/255.0f alpha:1.0]];
    [self.scroll addSubview:_photoContent];
    UILabel *myTitle = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 150, 40)];
    myTitle.text = _currentPhoto.title;
    myTitle.font = [UIFont boldSystemFontOfSize:20];
    myTitle.textColor = [UIColor whiteColor];
    myTitle.textAlignment = NSTextAlignmentCenter;
    self.navigationItem.titleView = myTitle;
    _slider = [[UISlider alloc] initWithFrame:CGRectMake(20, 450, 200, 50)];
    [self.view addSubview:_slider];
    [_slider addTarget:self action:@selector(zoom:) forControlEvents:UIControlEventValueChanged];
    _slider.minimumValue = 1.0f;
    _slider.maximumValue = 6.0f;
    _scroll.contentSize = _photoContent.frame.size;
    
    _switchBtn = [[UISwitch alloc] initWithFrame:CGRectMake(240, 455, 50, 50)];
    [_switchBtn addTarget:self action:@selector(block:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_switchBtn];
    _switchBtn.on = YES;
}

- (void)block:(UISwitch *)sender
{
    if (_switchBtn.on == YES)
    {
        _slider.enabled = YES;
    }
    else
    {
        _slider.enabled = NO;
    }
}

- (void)zoom:(UISlider *)sender
{
    [self.scroll setZoomScale:sender.value];
}

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView
{
    if (_switchBtn.on == YES)
        return _photoContent;
    return nil;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    _slider.value = _scroll.zoomScale;
}



@end
