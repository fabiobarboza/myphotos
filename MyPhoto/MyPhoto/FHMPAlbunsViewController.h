//
//  FHMPAlbunsViewController.h
//  MyPhoto
//
//  Created by Fabio Barboza on 1/20/14.
//  Copyright (c) 2014 Fabio Barboza. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FHMPPhoto.h"

@interface FHMPAlbunsViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic) NSMutableArray *albunsList;
@property (nonatomic) NSMutableArray *photoList1;
@property (nonatomic) NSMutableArray *photoList2;
@property (nonatomic) NSMutableArray *photoList3;
@property (nonatomic) NSMutableArray *photoList4;
@property (nonatomic) FHMPPhoto *currentPhoto;
@end
