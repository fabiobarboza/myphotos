//
//  FHMPPhotoViewController.h
//  MyPhoto
//
//  Created by Fabio Barboza on 1/20/14.
//  Copyright (c) 2014 Fabio Barboza. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FHMPPhoto.h"
#import "FHMPAlbum.h"

@interface FHMPPhotoViewController : UIViewController <UIScrollViewDelegate>
@property (nonatomic) FHMPAlbum *currentAlbum;
@property (nonatomic) FHMPPhoto *currentPhoto;


@end
