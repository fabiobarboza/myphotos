//
//  UIImageResizing.h
//  MyPhoto
//
//  Created by Fabio Barboza on 1/21/14.
//  Copyright (c) 2014 Fabio Barboza. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UIImage (Resize)
- (UIImage*)scaleToSize:(CGSize)size;
@end
