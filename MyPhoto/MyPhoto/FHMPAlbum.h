//
//  FHMPAlbum.h
//  MyPhoto
//
//  Created by Fabio Barboza on 1/21/14.
//  Copyright (c) 2014 Fabio Barboza. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FHMPAlbum : NSObject

@property (nonatomic) NSString *title;
@property (nonatomic) int albumId;
@property (nonatomic) NSMutableArray *photoList;
- (instancetype)initWithAlbumId:(int)anId andTitle:(NSString *)aTitle;

@end
