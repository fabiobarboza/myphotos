//
//  FHMPPhoto.m
//  MyPhoto
//
//  Created by Fabio Barboza on 1/20/14.
//  Copyright (c) 2014 Fabio Barboza. All rights reserved.
//

#import "FHMPPhoto.h"

@implementation FHMPPhoto

- (instancetype)init
{
    
    return [self initWithImage:nil andTitle:@""];
}

- (instancetype)initWithImage:(UIImage *)anImage andTitle:(NSString *)aTitle
{
    self = [super init];
    if (self) {
        self.image = anImage;
        self.title = aTitle;
    }
    return (self);
}

@end
