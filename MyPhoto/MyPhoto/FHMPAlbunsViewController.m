//
//  FHMPAlbunsViewController.m
//  MyPhoto
//
//  Created by Fabio Barboza on 1/20/14.
//  Copyright (c) 2014 Fabio Barboza. All rights reserved.
//

#import "FHMPAlbunsViewController.h"
#import "FHMPPhotoViewController.h"
#import "UIImageResizing.h"
#import "FHMPAlbum.h"

@interface FHMPAlbunsViewController ()
@property (weak, nonatomic) IBOutlet UITableView *albunsTable;
@property (nonatomic) int photoCount;
@property (nonatomic) int albumCount;

@end

@implementation FHMPAlbunsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
    self.view.backgroundColor = [UIColor colorWithRed:40/255.0f green:40/255.0f blue:40/255.0f alpha:1.0];
    self.albunsTable.backgroundColor = [UIColor colorWithRed:40/255.0f green:40/255.0f blue:40/255.0f alpha:1.0];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.albunsTable.delegate = self;
    self.albunsTable.dataSource = self;
//    [[scrollview subview] makeObjectsPerformSelector@selector(removeFromSuperview)];
    [self loadPhotos];
    UILabel *myTitle = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 150, 40)];
    myTitle.text = @"Albuns";
    myTitle.font = [UIFont boldSystemFontOfSize:20];
    myTitle.textColor = [UIColor whiteColor];
    myTitle.textAlignment = NSTextAlignmentCenter;
    self.navigationItem.titleView = myTitle;
    
}

- (void)loadPhotos
{
    _albunsList = [[NSMutableArray alloc] init];
    
    //Lista 1 - Marvel
    _photoList1 = [[NSMutableArray alloc] init];
    FHMPAlbum *album1 = [[FHMPAlbum alloc] initWithAlbumId:1 andTitle:@"Marvel"];
    
    
    UIImage *img1 = [UIImage imageNamed:@"foto1.png"];
    FHMPPhoto *photo1 = [[FHMPPhoto alloc] initWithImage:img1 andTitle:@"Homem de Ferro"];
    [_photoList1 addObject:photo1];
    
    UIImage *img2 = [UIImage imageNamed:@"foto2.jpg"];
    FHMPPhoto *photo2 = [[FHMPPhoto alloc] initWithImage:img2 andTitle:@"Homem-Aranha"];
    [_photoList1 addObject:photo2];
    
    UIImage *img3 = [UIImage imageNamed:@"foto3.jpg"];
    FHMPPhoto *photo3 = [[FHMPPhoto alloc] initWithImage:img3 andTitle:@"Hulk"];
    [_photoList1 addObject:photo3];
    
    UIImage *img4 = [UIImage imageNamed:@"foto4.jpg"];
    FHMPPhoto *photo4 = [[FHMPPhoto alloc] initWithImage:img4 andTitle:@"Wolverine"];
    [_photoList1 addObject:photo4];
    
    UIImage *img5 = [UIImage imageNamed:@"foto5.jpg"];
    FHMPPhoto *photo5 = [[FHMPPhoto alloc] initWithImage:img5 andTitle:@"Capitão América"];
    [_photoList1 addObject:photo5];
    
    album1.photoList = _photoList1;
    [_albunsList addObject:album1];
    
    //Lista 2 - Praia
    _photoList2 = [[NSMutableArray alloc] init];
    FHMPAlbum *album2 = [[FHMPAlbum alloc] initWithAlbumId:2 andTitle:@"Praias"];
    
    UIImage *img6 = [UIImage imageNamed:@"foto6.jpg"];
    FHMPPhoto *photo6 = [[FHMPPhoto alloc] initWithImage:img6 andTitle:@"Cancun"];
    [_photoList2 addObject:photo6];
    
    UIImage *img7 = [UIImage imageNamed:@"foto7.jpg"];
    FHMPPhoto *photo7 = [[FHMPPhoto alloc] initWithImage:img7 andTitle:@"Bora-Bora"];
    [_photoList2 addObject:photo7];
    
    UIImage *img8 = [UIImage imageNamed:@"foto8.jpg"];
    FHMPPhoto *photo8 = [[FHMPPhoto alloc] initWithImage:img8 andTitle:@"Hawai"];
    [_photoList2 addObject:photo8];
    
    UIImage *img9 = [UIImage imageNamed:@"foto9.jpg"];
    FHMPPhoto *photo9 = [[FHMPPhoto alloc] initWithImage:img9 andTitle:@"Ilhas Caiman"];
    [_photoList2 addObject:photo9];
    
    UIImage *img10 = [UIImage imageNamed:@"foto10.jpg"];
    FHMPPhoto *photo10 = [[FHMPPhoto alloc] initWithImage:img10 andTitle:@"Galápagos"];
    [_photoList2 addObject:photo10];
    
    album2.photoList = _photoList2;
    [_albunsList addObject:album2];
    
    //Lista 3 - Natureza
    _photoList3 = [[NSMutableArray alloc] init];
    FHMPAlbum *album3 = [[FHMPAlbum alloc] initWithAlbumId:3 andTitle:@"Natureza"];
    
    UIImage *img11 = [UIImage imageNamed:@"foto11.jpg"];
    FHMPPhoto *photo11 = [[FHMPPhoto alloc] initWithImage:img11 andTitle:@"Amazonia"];
    [_photoList3 addObject:photo11];
    
    UIImage *img12 = [UIImage imageNamed:@"foto12.jpg"];
    FHMPPhoto *photo12 = [[FHMPPhoto alloc] initWithImage:img12 andTitle:@"Panamá"];
    [_photoList3 addObject:photo12];
    
    UIImage *img13 = [UIImage imageNamed:@"foto13.jpg"];
    FHMPPhoto *photo13 = [[FHMPPhoto alloc] initWithImage:img13 andTitle:@"Afeganistão"];
    [_photoList3 addObject:photo13];
    
    UIImage *img14 = [UIImage imageNamed:@"foto14.jpg"];
    FHMPPhoto *photo14 = [[FHMPPhoto alloc] initWithImage:img14 andTitle:@"Piraporinha"];
    [_photoList3 addObject:photo14];
    
    UIImage *img15 = [UIImage imageNamed:@"foto15.jpg"];
    FHMPPhoto *photo15 = [[FHMPPhoto alloc] initWithImage:img15 andTitle:@"Porto Alegre"];
    [_photoList3 addObject:photo15];
    
    album3.photoList = _photoList3;
    [_albunsList addObject:album3];
    
    //Lista 4 - Star Wars
    _photoList4 = [[NSMutableArray alloc] init];
    FHMPAlbum *album4 = [[FHMPAlbum alloc] initWithAlbumId:3 andTitle:@"Star Wars"];
    
    UIImage *img16 = [UIImage imageNamed:@"foto16.jpg"];
    FHMPPhoto *photo16 = [[FHMPPhoto alloc] initWithImage:img16 andTitle:@"Padma"];
    [_photoList4 addObject:photo16];
    
    UIImage *img17 = [UIImage imageNamed:@"foto17.jpg"];
    FHMPPhoto *photo17 = [[FHMPPhoto alloc] initWithImage:img17 andTitle:@"C3PO"];
    [_photoList4 addObject:photo17];
    
    UIImage *img18 = [UIImage imageNamed:@"foto18.jpg"];
    FHMPPhoto *photo18 = [[FHMPPhoto alloc] initWithImage:img18 andTitle:@"R2D2"];
    [_photoList4 addObject:photo18];
    
    UIImage *img19 = [UIImage imageNamed:@"foto19.jpg"];
    FHMPPhoto *photo19 = [[FHMPPhoto alloc] initWithImage:img19 andTitle:@"Darth Vader"];
    [_photoList4 addObject:photo19];
    
    UIImage *img20 = [UIImage imageNamed:@"foto20.jpg"];
    FHMPPhoto *photo20 = [[FHMPPhoto alloc] initWithImage:img20 andTitle:@"Mestre Yoda"];
    [_photoList4 addObject:photo20];
    
    album4.photoList = _photoList4;
    [_albunsList addObject:album4];
    
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return _albunsList.count;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView * view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 50)];
    view.backgroundColor = [UIColor whiteColor];
    FHMPAlbum *album = _albunsList[section];
    UILabel * lbltitle = [[UILabel alloc] initWithFrame:CGRectMake(20, 10, 200, 30)];
    lbltitle.font = [UIFont systemFontOfSize:25.0f];
    lbltitle.textColor = [UIColor whiteColor];
    lbltitle.text = [NSString stringWithFormat:@"%@", album.title];
    view.backgroundColor = [UIColor colorWithRed:40/255.0f green:40/255.0f blue:40/255.0f alpha:1.0];
    [view addSubview:lbltitle];
    return view;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 50;
}

- (NSMutableArray *)getCurrentList:(int)listNumber
{
    NSMutableArray *aux;
    
    switch (listNumber)
    {
        case 0: aux = _photoList1;
            return aux;
        case 1: aux = _photoList2;
            return aux;
        case 2: aux = _photoList3;
            return aux;
        case 3: aux = _photoList4;
    }
    return aux;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    UIScrollView * scroll = (UIScrollView *)[cell viewWithTag:99];
    
    FHMPAlbum *album = _albunsList[indexPath.section];
    cell.backgroundColor = [UIColor whiteColor];
    int currX = 10;
    for (int i = 0; i < [album.photoList count]; i++)
    {
        FHMPPhoto *photoAux = album.photoList[i];
        UIButton *aux = [[UIButton alloc] initWithFrame:CGRectMake(currX, 10, 100, 100)];
        [aux setBackgroundImage:[photoAux.image scaleToSize:CGSizeMake(100.0f,100.0f)] forState:UIControlStateNormal];
        [aux addTarget:self action:@selector(nextPage:) forControlEvents:UIControlEventTouchUpInside];
        aux.tag = (indexPath.section * 10) + i;
        [aux.layer setBorderWidth:1.0f];
        [aux.layer setBorderColor:[UIColor grayColor].CGColor];
        aux.backgroundColor = [UIColor blackColor];
        UILabel *phototitle = [[UILabel alloc] initWithFrame:CGRectMake(currX, 112, 100, 15)];
        phototitle.textAlignment = NSTextAlignmentCenter;
        phototitle.textColor = [UIColor whiteColor];
        phototitle.text = photoAux.title;
        phototitle.font = [UIFont systemFontOfSize:12.0f];
        [scroll addSubview:phototitle];
        [scroll addSubview:aux];
        currX += aux.frame.size.width + 10;

    }
    
    [scroll setContentSize:CGSizeMake(currX, 139)];
    
    
    UILabel *line = [[UILabel alloc] initWithFrame:CGRectMake(10, 137, 300, 1)];
    line.backgroundColor = [UIColor whiteColor];
    [cell addSubview:line];
    
    cell.backgroundColor = [UIColor colorWithRed:54/255.0f green:54/255.0f blue:54/255.0f alpha:1.0];
    
    return cell;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(UITableViewCell *)sender
{
    NSIndexPath *idPath = [self.albunsTable indexPathForCell:sender];
//    NSMutableArray *arrayAux = [self getCurrentList:_albumCount];
    FHMPAlbum *album = _albunsList[_albumCount];
    NSLog(@"%d", idPath.section);
    _currentPhoto = (FHMPPhoto *)album.photoList[_photoCount];
    FHMPPhotoViewController * photoDetailVC = (FHMPPhotoViewController *) segue.destinationViewController;
    photoDetailVC.currentPhoto = _currentPhoto;
    photoDetailVC.currentAlbum = album;
}

- (int) checkAlbum:(int)senderTag
{
    if (senderTag < 10)
    {
        return 0;
    }
    if (senderTag > 9 && senderTag < 20)
    {
        _photoCount -= 10;
        return 1;
    }
    if (senderTag > 19 && senderTag < 30)
    {
        _photoCount -= 20;
        return 2;
    }
    if (senderTag > 29 && senderTag < 40)
    {
        _photoCount -= 30;
        return 3;
    }
    
    
    return 99;
}

- (IBAction)nextPage:(UIButton *)sender
{
    _photoCount = sender.tag;
    _albumCount = [self checkAlbum:sender.tag];
    [self performSegueWithIdentifier:@"photoPageSegue" sender:nil];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
