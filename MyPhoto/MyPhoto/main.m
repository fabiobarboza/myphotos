//
//  main.m
//  MyPhoto
//
//  Created by Fabio Barboza on 1/20/14.
//  Copyright (c) 2014 Fabio Barboza. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "FHMPAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([FHMPAppDelegate class]));
    }
}
