//
//  FHMPAlbum.m
//  MyPhoto
//
//  Created by Fabio Barboza on 1/21/14.
//  Copyright (c) 2014 Fabio Barboza. All rights reserved.
//

#import "FHMPAlbum.h"

@implementation FHMPAlbum

- (instancetype)init
{
    
    return [self initWithAlbumId:0 andTitle:@""];
}

- (instancetype)initWithAlbumId:(int)anId andTitle:(NSString *)aTitle
{
    self = [super init];
    if (self) {
        self.albumId = anId;
        self.title = aTitle;
        self.photoList = [[NSMutableArray alloc] init];
    }
    return (self);
}

@end
