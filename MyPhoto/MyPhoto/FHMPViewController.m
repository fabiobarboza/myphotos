//
//  FHMPViewController.m
//  MyPhoto
//
//  Created by Fabio Barboza on 1/20/14.
//  Copyright (c) 2014 Fabio Barboza. All rights reserved.
//

#import "FHMPViewController.h"

@interface FHMPViewController ()

@end

@implementation FHMPViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    //320
    //519
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"background.jpg"] forBarMetrics:UIBarMetricsDefault];
    [self addComponents];
}

- (void)addComponents
{
    UIImageView * bgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320, 568)];
    UIImage * bgImage = [UIImage imageNamed:@"background.jpg"];
    bgView.image = bgImage;
    [self.view addSubview:bgView];
    
    UIImageView * logoView = [[UIImageView alloc] initWithFrame:CGRectMake(60, 230, 200, 60)];
    UIImage * logo = [UIImage imageNamed:@"myphotos-logo2.png"];
    logoView.image = logo;
    [self.view addSubview:logoView];
    
    UIButton * btnNextPage = [[UIButton alloc] initWithFrame:CGRectMake(80, 310, 160, 40)];


    [btnNextPage setTitle:@"Albuns" forState:UIControlStateNormal];
    
    [btnNextPage setTitleColor: [UIColor colorWithRed:30/255.0f green:144/255.0f blue:255/255.0f alpha:1.0] forState:UIControlStateNormal];

    [btnNextPage addTarget:self action:@selector(nextPage:) forControlEvents:UIControlEventTouchUpInside];

    [self.view addSubview:btnNextPage];
}

- (IBAction)nextPage:(UIButton *)sender
{
    [self performSegueWithIdentifier:@"albunsPageSegue" sender:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
