//
//  FHMPPhoto.h
//  MyPhoto
//
//  Created by Fabio Barboza on 1/20/14.
//  Copyright (c) 2014 Fabio Barboza. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FHMPPhoto : NSObject

@property (nonatomic) UIImage *image;
@property (nonatomic) NSString *title;

- (instancetype)initWithImage:(UIImage *)anImage andTitle:(NSString *)aTitle;
@end
